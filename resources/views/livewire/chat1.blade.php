<div class="card">

    @if (session()->has('message'))
    <div class="alert alert-{{session('alert-class')}}" role="alert" style="opacity: .4;">
        {{ session('message') }}
        <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    @endif


    <div class="card-header">
        <h5 class="card-title">Usuario: {{ $usuario }}</h5>
    </div>
    <div class="card-body">
        <textarea wire:model='texto' name="" cols="40" rows="5"></textarea>
    </div>
    <div class="card-footer">
        <button wire:click='enviarMensaje' class="btn btn-primary">Enviar</button>
    </div>

</div>
