<div class="card">
    @if (session()->has('message'))
    <div class="alert alert-{{session('alert-class')}}" role="alert" style="opacity: .4;">
        {{ session('message') }}
        <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    @endif
    <div class="card-header">
        <h5 class="card-title">
            Conversacion:</h5>
    </div>
    <div class="card-body">
        <textarea wire:model='conversacion' name="" cols="100" rows="5"></textarea>
    </div>

    <div class="card-footer">
        <button wire:click='limpiarConversacion' class="btn btn-primary">Limpiar Conversacion</button>
    </div>
    <div class="card-footer">
        <button wire:click='limpiarConversacionComponentes' class="btn btn-primary">Limpiar Conversacion Componentes</button>
    </div>
</div>
