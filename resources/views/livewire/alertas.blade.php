@if (session()->has('message'))
    <div class="alert alert-{{session('alert-class')}}" role="alert" style="opacity: .4;">
        {{ session('message') }}
        <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    @endif
