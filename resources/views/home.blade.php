@extends('adminlte::page')

@section('content')
<div class="container">
    <div class="row justify-content-center">
       
        <div class="col-12">

            <div class="row">

                <div class="col-6">
                    @livewire('chat1')
                </div>

                <div class="col-6">
                    @livewire('chat2')
                </div>

                <div class="col-12">
                    @livewire('conversacion')
                </div>

            </div>


        </div>
    </div>
</div>
@endsection
