<?php

namespace App\Http\Livewire;

use Livewire\Component;

class Chat1 extends Component
{
    public $usuario;
    public $texto;

    protected $listeners = [
        'borrarChat' => 'limpiarChat',
    ];

    public function mount()
    {
        $this->usuario = 'Jose';
    }

    public function limpiarChat()
    {
        $this->texto = "";
    }

    public static function mostrarMensaje($tipo, $mensaje)
    {
        session()->flash('message', $mensaje);
        session()->flash('alert-class', $tipo);
    }

    public function enviarMensaje()
    {
        $this->emitTo('conversacion', 'mensajeEnviado', $this->usuario, $this->texto);
        $this->texto = "";
        $this->mostrarMensaje('success',"Mensaje enviado correctamente.");
    }

    public function render()
    {
        return view('livewire.chat1');
    }
}
