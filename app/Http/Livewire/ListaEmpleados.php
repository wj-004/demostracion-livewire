<?php

namespace App\Http\Livewire;

use App\Models\User;
use Livewire\Component;

class ListaEmpleados extends Component
{
    public $usuarios;
    public $nuevo_usuario;

    public function mount()
    {
        $this->usuario_1 = null;
        $this->usuario_2 = null;
        $this->nuevo_usuario = null;
        $this->usuarios = $this->getUsuarios();
    }

    public function getUsuarios()
    {
        $usuarios = User::get()->all();
        return $usuarios;
    }

    public function render()
    {
        return view('livewire.lista-empleados');
    }
}
