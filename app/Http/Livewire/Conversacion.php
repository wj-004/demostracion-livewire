<?php

namespace App\Http\Livewire;

use App\Models\User;
use Livewire\Component;

class Conversacion extends Component
{
    public $conversacion;
    public $usuario;

    protected $listeners = [
        'mensajeEnviado' => 'actualizaConversacion',
    ];


    public function actualizaConversacion(User $usuario = null, string $texto = null)
    {
        $this->usuario = $usuario;
        $this->conversacion = $this->manejadorTexto($texto);
    }

    public function manejadorTexto($texto)
    {
        $mensaje = $this->usuario
        . ": "
        . "\n      ". $texto
        . "\n";
        return $this->conversacion . $mensaje;

    }

    public function limpiarConversacion()
    {
        $this->conversacion = "";
    }

    public function limpiarConversacionComponentes()
    {
        $this->limpiarConversacion();
        $this->emit('borrarChat');
        $this->mostrarMensaje('danger',"Mensajes eliminados");
    }

    public static function mostrarMensaje($tipo, $mensaje)
    {
        session()->flash('message', $mensaje);
        session()->flash('alert-class', $tipo);
    }

    public function mount()
    {
        $this->conversacion = "";
    }
    public function render()
    {
        return view('livewire.conversacion');
    }
}
