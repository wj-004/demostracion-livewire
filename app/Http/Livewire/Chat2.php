<?php

namespace App\Http\Livewire;

use Livewire\Component;

class Chat2 extends Component
{
    public $usuario;
    public $texto;

    protected $listeners = [
        'borrarChat' => 'limpiarChat',
    ];

    public function mount()
    {
        $this->usuario = 'Julien';
    }

    public function limpiarChat()
    {
        $this->texto = "";
    }


    public function enviarMensaje()
    {
        $this->emitTo('conversacion', 'mensajeEnviado', $this->usuario,$this->texto);
        $this->texto = "";
        $this->mostrarMensaje('success',"Mensaje enviado");
    }

    public static function mostrarMensaje($tipo, $mensaje)
    {
        session()->flash('message', $mensaje);
        session()->flash('alert-class', $tipo);
    }

    public function render()
    {
        return view('livewire.chat2');
    }
}
